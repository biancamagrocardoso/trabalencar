package br.csi.controller;

import br.csi.model.Usuario;
import br.csi.service.UsuarioService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("controlador")

public class DashboardController extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        System.out.println("**************");

        System.out.println("user-agent "+req.getHeader("User-Agent"));

        System.out.println("**************");

        String opcao = req.getParameter("opcao");

        System.out.println("Opção de navegação: "+opcao);

        RequestDispatcher rd = req.getRequestDispatcher("/");

        if (req.getSession().getAttribute("usuario_logado") != null) {


            rd = req.getRequestDispatcher("/WEB-INF/Home/dashboard.jsp");

            if (opcao.equals("cliente")) {
                rd = req.getRequestDispatcher("/WEB-INF/Home/Cliente.jsp");

            } else if (opcao.equals("produto")) {
                rd = req.getRequestDispatcher("/WEB-INF/Home/Produto.jsp");

            } else if(opcao.equals("usuario")){
                rd = req.getRequestDispatcher("/WEB-INF/Home/Usuario.jsp");

            } else if (opcao.equals("sair")) {
                req.getSession().invalidate();
                rd = req.getRequestDispatcher("/");
            }

        }
            rd.forward(req, resp);

    }
}
