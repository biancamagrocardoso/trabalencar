package br.csi.controller;

import br.csi.model.Usuario;
import br.csi.service.UsuarioService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("cad")

public class UsuarioController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nome = req.getParameter("nome");
        String email = req.getParameter("email");
        String senha = req.getParameter("senha");

        RequestDispatcher rd = null;

        System.out.println(nome);
        System.out.println(email);
        System.out.println(senha);

        Usuario u = new Usuario();
        u.setNome(nome);
        u.setEmail(email);
        u.setSenha(senha);

        String teste = new UsuarioService().cadastro(u);

        if(teste == "erro"){
            req.setAttribute("erro","Todos campos devem ser preenchidos!");
            rd = req.getRequestDispatcher("/WEB-INF/cadastro.jsp");

        } else if(teste == "email"){
            req.setAttribute("erro","Email ja cadastrado!");
            rd = req.getRequestDispatcher("/WEB-INF/cadastro.jsp");

        }else if(teste == "certo"){
            rd = req.getRequestDispatcher("/WEB-INF/login.jsp");

        }
        rd.forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher rd;
        rd = req.getRequestDispatcher("/WEB-INF/cadastro.jsp");
        rd.forward(req, resp);
    }
}