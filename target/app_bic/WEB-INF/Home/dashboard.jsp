<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>Olá ${usuario_logado.nome}</h1>

<h2>ID da sessão: ${pageContext.session.id}</h2>
<h3>Criação da sessão: ${pageContext.session.creationTime}</h3>
<h3>Ultimo acesso: ${pageContext.session.lastAccessedTime}</h3>

<ul>
    <li><a href="controlador?opcao=cliente">Cadastrar Cliente</a></li>
    <li><a href="controlador?opcao=produto">Cadastrar Prduto</a></li>
    <li><a href="controlador?opcao=usuario">Usuario</a></li>

    <c:if test="${not empty erro}">
        <li> <a href="controlador?opcao=usuarios">${erro}</a> </li>
    </c:if>

    <li><a href="controlador?opcao=sair">Sair</a></li>
</ul>

</body>
</html>
